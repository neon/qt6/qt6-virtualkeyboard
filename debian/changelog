qt6-virtualkeyboard (6.8.2-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 09 Feb 2025 09:57:58 +1000

qt6-virtualkeyboard (6.8.1-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 08 Dec 2024 08:30:43 +1000

qt6-virtualkeyboard (6.8.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Wed, 24 Jul 2024 21:37:20 +1000

qt6-virtualkeyboard (6.7.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sat, 17 Feb 2024 00:28:09 +1000

qt6-virtualkeyboard (6.6.1-0neon) jammy; urgency=medium

  [ Patrick Franz ]
  * New release

  [ Carlos De Maine ]
  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 10 Dec 2023 18:36:01 +1000

qt6-virtualkeyboard (6.4.2+dfsg-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Rename libqt6virtualkeyboard6-dev to qt6-virtualkeyboard-dev.
  * Update d/copyright.

 -- Patrick Franz <deltaone@debian.org>  Wed, 30 Nov 2022 23:42:01 +0100

qt6-virtualkeyboard (6.4.2+dfsg-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.4.2).
  * Bump Qt B-Ds to 6.4.2.

 -- Patrick Franz <deltaone@debian.org>  Sat, 19 Nov 2022 01:17:56 +0100

qt6-virtualkeyboard (6.4.0+dfsg-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Enable link time optimization (Closes: #1015626).

 -- Patrick Franz <deltaone@debian.org>  Sat, 12 Nov 2022 14:11:28 +0100

qt6-virtualkeyboard (6.4.0+dfsg-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Bump minimum CMake version in order to get pkg-config's .pc files.

  [ Patrick Franz ]
  * Increase CMake verbosity level.
  * New upstream release (6.4.0).
  * Bump Qt B-Ds to 6.4.0.
  * Adjust patches.
  * Update list of installed files.
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Fri, 14 Oct 2022 23:18:03 +0200

qt6-virtualkeyboard (6.3.1+dfsg-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Mon, 15 Aug 2022 19:24:04 +0200

qt6-virtualkeyboard (6.3.1+dfsg-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.1).
  * Bump Qt B-Ds to 6.3.1.
  * Bump Standards-Version to 4.6.1 (no changes needed).
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Sun, 17 Jul 2022 19:07:04 +0200

qt6-virtualkeyboard (6.3.0+dfsg-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.0).
  * Bump Qt B-Ds to 6.3.0.
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Mon, 20 Jun 2022 02:31:59 +0200

qt6-virtualkeyboard (6.2.4+dfsg-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Fri, 22 Apr 2022 20:07:46 +0200

qt6-virtualkeyboard (6.2.4+dfsg-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.2.4).
  * Bump Qt B-Ds to 6.2.4.

 -- Patrick Franz <deltaone@debian.org>  Thu, 31 Mar 2022 23:10:20 +0200

qt6-virtualkeyboard (6.2.2+dfsg-3) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 15 Feb 2022 22:50:25 +0100

qt6-virtualkeyboard (6.2.2+dfsg-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Source-only upload to experimental.

 -- Patrick Franz <deltaone@debian.org>  Thu, 30 Dec 2021 16:24:04 +0100

qt6-virtualkeyboard (6.2.2+dfsg-1) experimental; urgency=medium

  * Initial release (Closes: #999897)

 -- Patrick Franz <deltaone@debian.org>  Thu, 18 Nov 2021 09:01:30 +0100
